# Ep3 - Orientação a objetos

	A aplicação anexada tem como proposta o gerenciamento de uma conta e suas atividades, relacionadas ao mercado de ativos futuros. Contem em sua composição três classes usuário, conta e negociação. Para que a aplicação funcione corretamente na maquina e aconselhável usar as mesma configurações do ambiente de desenvolvimento, listadas abaixo.

* Ruby version: ruby 2.3.1p112

* Rails version: rails 5.2.3

	Apos, acertada a compatibilidade, execute dentro do diretorio:

1)rails db:create
2)rails db:migrate
3)rails db:seed
4)rails server

	A aplicação se estabiliza em "localhost:3000", podendo ser acessada por algum browser. Para navegar pela aplicação com o usuário admin da mesma, use as credenciais listadas abaixo:

usuario: manager@teste.com
senha: 123456
