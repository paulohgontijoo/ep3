class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.date :action_date
      t.decimal :value
      t.integer :kind

      t.timestamps
    end
  end
end
