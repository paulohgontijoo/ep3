class CreateTrades < ActiveRecord::Migration[5.2]
  def change
    create_table :trades do |t|
      t.string :cod
      t.date :action_date
      t.decimal :value
      t.integer :kind

      t.timestamps
    end
  end
end
